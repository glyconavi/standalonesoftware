# Repository 

* Git repository is https://gitlab.com/glyconavi/pdb2glycan

# Version
* Version: 0.18.0

# PDB2Glycan

* This software outputs the result of analysis of glycan information about PDB's mmJSON and mmCIF format data.
* The results can be output as JSON, JSONLD, or Turtle format, and each file is gzip-compressed by default.

## Requirements
* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher

## Usage

    $ bin/PDB2Glycan [--no-network --pdb <DIRECTORY> --cc <DIRECTORY> --local-glytoucan <FILE>]
                     [--force] [--nozip] [--dir <DIRECTORY>]  --in <FORMAT> --out <FORMAT> <PDB_4_LETTER_CODE>
    
    or
    
    $ cd target
    $ java -jar PDB2Glycan.jar [--no-network --pdb <DIRECTORY> --cc <DIRECTORY> --local-glytoucan <FILE>]
                     [--force] [--nozip] [--dir <DIRECTORY>]  --in <FORMAT> --out <FORMAT> <PDB_4_LETTER_CODE>


### Options

| option | argument | description |
| ------ | -------- | ----------- |
| --cc              | DIRECTORY                     | Set path to the directory where PDB Chemical Component Dictionary is placed |
| -d, --dir         | DIRECTORY                     | Set path to output directory |
| -f, --force       |                               | Overwrite existing file |
| -h, --help        |                               | Show usage help |
| -i, --in          | FORMAT=[mmcif\|mmjson]        | Set input format, required |
| --local-glytoucan | FILE                          | Set path to the file of local mappings for WURCS to GlyTouCan ID |
| --no-network      |                               | Use local data, all of --cc <DIRECTORY>, --pdb <DIRECTORY> and --local-glytoucan <FILE> are required |
| --nozip           |                               | Do not zip output file |
| -o, --out         | FORMAT=[json\|jsonld\|turtle] | Set output format, required |
| --pdb             | DIRECTORY                     | Set path to the directory where PDB entry is placed |


#### Note

If you would like to use this program in an environment disconnected from the internet,
please generate ID mappings manually by using `bin/LocalGlyTouCanID` and pass the file to argument of `--local-glytoucan`.

    $ bin/LocalGlyTouCanID > glytoucan.tsv


### Example

Fetch data from remote

    $ java -jar PDB2Glycan.jar --in mmjson --out turtle 1BWU


Use local files

    $ java -jar PDB2Glycan.jar --no-network --pdb /path/to/mmjson/direcotry --cc /path/to/cc/directory --local-glytoucan /path/to/glytoucan.tsv --in mmjson --out jsonld 1BWU

## Messages in data loading
### INFO

* ```Load <PDB file name> from <Host URL>```  
The PDB file is loaded from the remote host.

* ```<PDB code> has no glyco entries```  
There is no glyco entries in the PDB data.

* ```<PDB code> finished in <time>```  
Processing the PDB data is finished in the time.

* ```Load <CCD file name> from <Host URL>```  
The CCD file is loaded from the remote host.

* ```GET <URL of WURCS2GlyTouCan api>```  
The GET process which run WURCS2GlyTouCan api is started.

* ```Successfully loaded <file path of GlyTouCanID.tsv>```  
The GlyTouCanID.tsv file is successfully loaded.

### WARN

* ```<connection error message>```  
```Retry after <time> [s]```  
A connection error is occured during remote access for finding CCD or PDB file from PDBj remote service.
The connection will be re-tried until data is found or the process is stopped due to the other error.

* ```CCD file with format "<used format>" different from the input format "<input format>" is used for "<shaccaride id in CCD>"```  
The CCD file format different from the input format is used.
It is happen when the CCD file for the given shaccaride ID with the input format could not find, but the other file with the different format is found.

* ```Failed to optain remote resource, use default data```  
An error is occured when CCD files are not optained from remote resource. Default CCD data is used when it is occured.

### ERROR

* ```Error validating the structure - <error message>```  
An error is occured in the validation of glycan structure using GlycomeDB validation is found.

* ```<error message>```  
An error in loading or parsing input files.